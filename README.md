# Configuration zone DNS alternatiba.eu pour le site web du Tour Alternatiba 2024

- L'application est sur nausicaa (Alternatiba) à l'adresse tour.alternatiba.eu
- La recette est sur ouaaa-recette (Aunis en transition) à l'adresse beta.tour.alternatiba.eu

Config DNS production (temporaire) :
```
nausicaa               3600  IN A      193.23.164.139
nausicaa               3600  IN AAAA   2a10:a080:1101:3900::1
prod.tour              3600  IN CNAME  nausicaa.alternatiba.eu.
api.tour               3600  IN CNAME  nausicaa.alternatiba.eu.
dbadmin.tour           3600  IN CNAME  nausicaa.alternatiba.eu.
static.tour            3600  IN CNAME  nausicaa.alternatiba.eu.
```

Config DNS production :
```
nausicaa               3600  IN A      193.23.164.139
nausicaa               3600  IN AAAA   2a10:a080:1101:3900::1
tour                   3600  IN CNAME  nausicaa.alternatiba.eu.
api.tour               3600  IN CNAME  tour.alternatiba.eu.
dbadmin.tour           3600  IN CNAME  tour.alternatiba.eu.
static.tour            3600  IN CNAME  tour.alternatiba.eu.
```

Config DNS recette :
```
beta.tour              3600  IN A      162.19.24.17
beta.tour              3600  IN AAAA   2001:41d0:304:100::43db
api.beta.tour          3600  IN CNAME  beta.tour.alternatiba.eu.
dbadmin.beta.tour      3600  IN CNAME  beta.tour.alternatiba.eu.
static.beta.tour       3600  IN CNAME  beta.tour.alternatiba.eu.
```
